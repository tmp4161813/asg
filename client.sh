#!/bin/sh
var1=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
var2=$(dig +short consul1.akimov.space)
arr1=$( jo -a $var2 )
json=$( jo -p advertise_addr="$var1" start_join="$arr1" enable_local_script_checks=true server=false datacenter=dc1 encrypt=86mYmtWxOA6fYjU+lM0pesQ3tl3FzEQIfVDJteu2EfU= leave_on_terminate=true log_level=INFO enable_syslog=true)
printf '%s\n' "$json" > client.json