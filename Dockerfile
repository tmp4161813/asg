# syntax = docker/dockerfile:1-experimental
FROM golang:1.16.6-alpine3.14 as builder
RUN mkdir /build
WORKDIR /build
COPY . . /build/

#caching dependencies
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -o app cmd/server/app.go
COPY scripts/fmt_in_docker.sh .
COPY scripts/test_in_docker.sh .

EXPOSE 8080

CMD ["./app"]
